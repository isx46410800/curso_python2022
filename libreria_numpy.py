#!/usr/bin/python3

# importamos la liberia
import numpy as np

# creamos arrays
lista1 = [1,2,3,4]
lista2 = [5,6,7,8]

print(np.zeros(4)) # crea array de 4 ceros
print(np.ones(5)) # crea array de 5 unos
np.arange(5) # crea array de 0 a 4
np.arange(2,10)

array1 = np.array(lista1)
array2 = np.array([1,2,3,4,5,6])
print(array1)

# array de dos dimensiones
lista_doble = (lista1,lista2,lista2,lista1)
print(lista_doble)

array_doble = np.array(lista_doble)
print(array_doble)

# formas que tiene el array
print(array_doble.shape) # 4 filas y 4 columnas
print(array_doble.dtype) # numeros int


# operaciones con arrays
print(array1 + 4) # multiplica todo lo de dentro por 4 pero no queda asignado
array_multiplicado = array1 * 3
print(array_multiplicado)

# indexacion / slicing
array3 = np.arange(10)
print(array3)
print(array3[0:3])
array4 = np.copy(array3)
array4[0:4] = 20
print(array3,array4)

# matrices
array5 = np.arange(15).reshape(3,5) # 3 filas y 5 columnas
print(array5)
print(array5[1][1])
array6 = array5.T # cambio el orden de filas y columnas
print(array6)

# entrada / salida de arrays
array7 = np.arange(6)
np.save("array7", array7) # guarda ese array con ese nombre
print(np.load("array7.npy")) # carga el archivo con el nombre del array
array8 = np.arange(8)
np.savez("arrayxy", x=array7, y=array8) # guarda en tipo coordenadas x y
array_xy = np.load("arrayxy.npz")
print(array_xy)
print(array_xy["x"])
print(array_xy["y"])
np.savetxt("mifilearray.txt", array8, delimiter=",") # guarda en un file txt
print(np.loadtxt("mifilearray.txt", delimiter=","))

# funciones
print(np.sqrt(array1)) #raiz cuadrada
print(np.random(5)) # 5 numeros aleatorios
print(np.add(array1, array2)) # suma las dos
print(np.maxium(array1,array2)) # elije el maximo de entre las dos arrays

# funcion par e impar
def pares(inicio,fin):
    if inicio % 2 == 0:
        array10 = np.arange(inicio,fin,2)
    else:
        inicio +=1
        array10 = np.arange(inicio,fin)
    return array10
print(pares(0,21))
