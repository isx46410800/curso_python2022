from selenium import webdriver
from selenium.webdriver.common.by import By
import time

driver = webdriver.Chrome(executable_path="Drivers/chromedriver.exe")
driver.get("https://www.udemy.com/join/login-popup/?skip_suggest=1&locale=es_ES&next=https%3A%2F%2Fwww.udemy.com%2Fmobile%2Fipad%2F&response_type=html")
time.sleep(1)

# elements genera una lista con el mismo atributo
varios = driver.find_elements(By.CSS_SELECTOR,"form-control")
for i in varios:
    time.sleep(1)
    i.send_keys("123456789")

driver.quit()