# importamos la implementacion que crea una instancia para conectarse a un navegador
from selenium import webdriver
from selenium.webdriver.common.by import By
# ID = "ID"
# XPATH = "XPATH"
# LINK_TEXT = "LINK TEXT"
# PARTIAL_LINK_TEXT = "PARTIAL LINK TEXT"
# NAME = "NAME"
# TAG_NAME = "TAG NAME"
# CLASS_NAME = "CLASS NAME"
# CSS_SELECTOR = "CSS SELECTOR"

# importamos libreria time
import time

# llamamos al driver y su path para abrir chrome (mejor llamar desde ruta original)
#driver = webdriver.Chrome(executable_path="Drivers/chromedriver")
controlador = webdriver.Chrome(executable_path=r"/home/miguel/Downloads/chromedriver_linux64/chromedriver")

# maximiza la ventana total del navegador
controlador.maximize_window()

# metodo get nos indica la url a la que conectarnos
controlador.get("https://www.udemy.com/join/login-popup/?locale=en_US&response_type=html&next=https%3A%2F%2Fwww.udemy.com%2Fjoin%2Flogin-popup%2F%3Flocale%3Des_ES%26response_type%3Dhtml%26next%3Dhttps%253A%252F%252Fwww.udemy.com%252Fes%252F%253Futm_source%253Dadwords-brand%2526utm_medium%253Dudemyads%2526utm_campaign%253DNEW-AW-PROS-Branded-Search-SP-SPA_._ci__._sl_SPA_._vi__._sd_All_._la_SP_._%2526tabei%253D7%2526utm_term%253D_._ag_53604040718_._ad_254061738916_._de_c_._dm__._pl__._ti_kwd-357002749620_._li_1005424_._pd__._%2526gclid%253DCj0KCQjw-uH6BRDQARIsAI3I-Ud3hC1QNzFFLCPuZ6H6BbB4sNh5StLf3qvjF1S-mVR0WaM8fs7gOeEaAr_HEALw_wcB%2526persist_locale%253D%2526locale%253Des_ES&persist_locale=")
time.sleep(3)

# miramos el link de recuperar contraseña y clicamos enlace
link_recuperacion = controlador.find_element(By.LINK_TEXT, "Forgot Password")
link_recuperacion.click()
time.sleep(3)

# Al poner que olvidamos contraseña ahora nos pide un correo y enviamos datos
correo = controlador.find_element(By.ID, "form-element--1")
correo.send_keys("dfdflujogramas@gmail.com")
time.sleep(3)

# validamos captcha google (NO SE PUEDE AUTOMATIZAR CAPTCHAS, FALLA)
validar = controlador.find_element(By.ID, "recaptcha-checkbox-border")
validar.click()

# luego hay que darle al boton de resetear password
boton = controlador.find_element(By.CLASS_NAME, "btn-primary")
boton.click()

# cerramos
controlador.quit()


