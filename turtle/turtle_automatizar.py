#!/usr/bin/python
# importamos la libreria turtle
import turtle

# creamos la pantalla
s = turtle.Screen()
# necesitamos el objeto, la tortuga a dibujar
t = turtle.Turtle()

# dar movimientos a la tortuga con un cuadrado
t.forward(100)
t.right(90)
t.forward(100)
t.right(90)
t.forward(100)
t.right(90)
t.forward(100)

# cuadrado automatizado
t.color("red","blue")
for i in range(4):
    t.forward(100)
    t.right(90)

# dar movimientos con un circulo
t.color("blue","yellow")
t.circle(100)
t.circle(80)
t.circle(60)
t.circle(40)
t.circle(20)

# circulo automatizado
resultado = input("Quieres dibujar?: ")
t.color("red","blue")
if resultado == "si":
    while i<=100:
        t.circle(i)
        i+=20
else:
    print("No quieres dibujar...:(")

# para que se quede la pantalla todo el rato
turtle.done()
