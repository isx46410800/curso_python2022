#!/usr/bin/python
# importamos la libreria turtle
import turtle
import random

# creamos la pantalla
s = turtle.Screen()

# color de la pantalla
s.bgcolor("cyan")
# nombre de la pestaña
s.title("Proyecto carrera tortugas")

# necesitamos el objeto, la tortuga a dibujar
jugador1 = turtle.Turtle()
jugador2 = turtle.Turtle()

# escondemos tortugas para dibujar el escenario
jugador1.hideturtle()
jugador2.hideturtle()

# personalizamos la tortuga forma,color,tinta,etc
jugador1.shape("turtle") # arrow, triangle, classic, circle
jugador1.color("green","green") # tinta/lienzo y relleno
jugador1.shapesize(2,2,2)
jugador1.pensize(3)
jugador2.shape("turtle") # arrow, triangle, classic, circle
jugador2.color("red","red") # tinta/lienzo y relleno
jugador2.shapesize(2,2,2)
jugador2.pensize(3)

# dibujando la meta de las tortugas
jugador1.penup()
jugador1.goto(300,200)
jugador1.pendown()
jugador1.circle(40)
jugador2.penup()
jugador2.goto(300,-200)
jugador2.pendown()
jugador2.circle(40)

# inicio de las tortugas y mostramos las tortugas
jugador1.penup()
jugador1.goto(-300,225)
jugador2.penup()
jugador2.goto(-300,-175)
jugador1.showturtle()
jugador2.showturtle()

# creamos el dado
dado = [1,2,3,4,5,6]

# movimiento de la tortuga
for i in range(20):
    if jugador1.pos() >= (300,200):
        print("El jugador 1 ha ganado!")
        break
    elif jugador2.pos() >= (300,200):
        print("El jugador 2 ha ganado!")
        break
    else:
        turno1 = input("JUGADOR1 presiona la tecla ENTER para avanzar.")
        turno1 = random.choice(dado)
        print("Tu numero es: ",turno1,"\nAvanzas: ",turno1*20)
        jugador1.pendown()
        jugador1.forward(turno1*20)

        turno2 = input("JUGADOR 2 presiona la tecla ENTER para avanzar.")
        turno2 = random.choice(dado)
        print("Tu numero es: ",turno2,"\nAvanzas: ",turno2*20)
        jugador2.pendown()
        jugador2.forward(turno2*20)

# para que se quede la pantalla todo el rato
turtle.done()
