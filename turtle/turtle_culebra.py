#!/usr/bin/python
# importamos la libreria turtle
import turtle
import random
import time

# creamos un delay en el movimiento
retraso = 0.1

# creamos la pantalla
s = turtle.Screen()
s.setup(650,650)
s.title("Proyecto juego de la serpiente")
s.bgcolor("cyan")

# necesitamos el objeto, la tortuga a dibujar
serpiente = turtle.Turtle()

# velocidad serpiente
serpiente.speed(1)

# personalizamos la serpiente
serpiente.shape("square") # arrow, triangle, classic, circle
serpiente.penup()
serpiente.goto(0,0)
serpiente.direction = "stop"
serpiente.color("green") 

# creamos la comida
comida = turtle.Turtle()
comida.shape("circle")
comida.color("orange")
comida.penup()
comida.goto(0,100)
comida.speed(0)

# creamos nuevo cuerpo
cuerpo = []

# creamos texto de los marcadores
marcador = 0
marcador_alto = 0
texto = turtle.Turtle()
texto.speed(0)
texto.color("black")
texto.penup()
texto.hideturtle()
texto.goto(0,-260)
texto.write("Marcador: 0\tMarcador más alto: 0", align="center", font=("verdana",14,"normal"))

# creamos funciones de teclas
def arriba():
    serpiente.direction = "up"
def abajo():
    serpiente.direction = "down"
def derecha():
    serpiente.direction = "right"
def izquierda():
    serpiente.direction = "left"

# creamos funcion movimiento
def movimiento():
    if serpiente.direction == "up":
        #devuelve la coordenada Y
        y = serpiente.ycor()
        # movemos la coordenada
        serpiente.sety(y + 20)
    if serpiente.direction == "down":
        #devuelve la coordenada Y
        y = serpiente.ycor()
        # movemos la coordenada
        serpiente.sety(y - 20)
    if serpiente.direction == "right":
        #devuelve la coordenada x
        x = serpiente.xcor()
        # movemos la coordenada
        serpiente.setx(x + 20)
    if serpiente.direction == "left":
        #devuelve la coordenada x
        x = serpiente.xcor()
        # movemos la coordenada
        serpiente.setx(x - 20)

# asignamos las teclas de flechas a la funcion
## ponemos a escuchar la pantalla
s.listen()
## apretar la tecla y vinculamos a la funcion
s.onkeypress(arriba, "Up")
s.onkeypress(abajo, "Down")
s.onkeypress(izquierda, "Left")
s.onkeypress(derecha, "Right")


# damos movimiento a la serpiente y a la comida
while True:
    s.update()

    # si se sobresale de la pantalla, se reinicia el cuerpo y se va a inicio
    if serpiente.xcor()>300 or serpiente.xcor()<-300 or serpiente.ycor()>300 or serpiente.ycor()<-300:
        time.sleep(2)
        for i in cuerpo:
            i.clear()
            i.hideturtle()
        serpiente.home()
        serpiente.direction = "stop"
        cuerpo.clear()
        marcador = 0
        texto.clear()
        texto.write(f"Marcador: {marcador}\tMarcador más alto: {marcador_alto}", align="center", font=("verdana",14,"normal"))


    # si distancia de serpiente y comida es menos de 20,comida se mueve a otro lado
    if serpiente.distance(comida) < 20:
        x = random.randint(-250,250)
        y = random.randint(-250,250)
        comida.goto(x,y)
        # nuevo tamaño serpiente despues de comer
        nuevo_cuerpo = turtle.Turtle()
        nuevo_cuerpo.shape("square")
        nuevo_cuerpo.color("green")
        nuevo_cuerpo.penup()
        nuevo_cuerpo.goto(0,0)
        nuevo_cuerpo.speed(0)
        cuerpo.append(nuevo_cuerpo)
        # añadimos marcador el punto
        marcador +=10
        if marcador>marcador_alto:
            marcador_alto=marcador
            texto.clear()
            texto.write(f"Marcador: {marcador}\tMarcador más alto: {marcador_alto}", align="center", font=("verdana",14,"normal"))


    # añadimos la lista de comida al cuerpo de la serpiente
    total = len(cuerpo)
    for i in range(total -1,0,-1):
        x = cuerpo[i-1].xcor()
        y = cuerpo[i-1].ycor()
        cuerpo[i].goto(x,y)
    
    if total > 0:
        x = serpiente.xcor()
        y = serpiente.ycor()
        cuerpo[0].goto(x,y)

    # se realiza movimientos
    movimiento()

    # si se toca el cuerpo se reinicia
    for i in cuerpo:
        if i.distance(serpiente) < 20:
            for i in cuerpo:
                i.clear()
                i.hideturtle()
            serpiente.home()
            cuerpo.clear()
            serpiente.direction = "stop"

    # tiempo de refresco de la pantalla.        
    time.sleep(retraso)

# para que se quede la pantalla todo el rato
turtle.done()
