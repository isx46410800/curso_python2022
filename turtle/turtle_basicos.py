#!/usr/bin/python
# importamos la libreria turtle
import turtle

# creamos la pantalla
s = turtle.Screen()

# color de la pantalla
s.bgcolor("red")
# nombre de la pestaña
s.title("Proyecto basicos turtle")
# configurar un tamaño de pantalla concreto
s.setup(750,750)

# necesitamos el objeto, la tortuga a dibujar
t = turtle.Turtle()
# personalizamos la tortuga forma,color,tinta,etc
t.shape("turtle") # arrow, triangle, classic, circle
t.shapesize(2,2,1)
t.fillcolor("orange")
t.pencolor("white")
t.color("green","blue") # borde y relleno
t.pensize(5)

# rellenar figuras
t.begin_fill()
t.color("white","blue") # borde/tinta y relleno
t.circle(100)
t.end_fill()

# dar velocidad a la tortuga (1-10)
t.speed(1)

# dar movimientos a la tortuga
t.backward(100)
t.right(90)
t.forward(100)
t.left(90)
t.forward(100)

# dar movimiento sin pintar
t.penup()
t.forward(50)
t.pendown()
t.forward(50)

# hacer un retroceso
t.undo()

# limpiar pantalla y resetear posicion
t.clear()
t.reset()

# dejar una marca como sello y seguir
t.forward(100)
t.stamp()
t.forward(100)

# movimiento perpendiculares
t.goto(100,100)
t.goto(-100,100)
t.goto(0,0) # == t.home())

# movimientos de formas
t.circle(50) #circulo diametro
t.dot(30) #punto y diametro

# esconder y mostrar de nuevo la tortuga dibujando
t.hideturtle()
t.circle(50)
t.showturtle()
t.circle(30)

# movilizar la tortuga
t.setx(100)
t.sety(-10)

# ver la posicion de coordenadas de la tortuga
print(t.pos())

# para que se quede la pantalla todo el rato
turtle.done()
