#!/usr/bin/python3

# CREAR FUNCIONES

## FUNCION SALUDO
def saludo(mensaje):
    print(mensaje)

saludo("Buenas tardes Miguel")
saludo("Buenas tardes Natalia")
saludo("Buenas tardes Isabel")

## FUNCION TABLA MULTIPLICAR
def tablaMultiplicar(numero, rango):
    for i in range(rango+1):
        print(f'{numero} X {i} = {numero+i}')

tablaMultiplicar(7,2)
tablaMultiplicar(10,3)

## FUNCION AÑADIR LISTA Y ORDENARLA
def ordenarLista(lista):
    par=[]
    impar=[]
    lista.sort()
    for x in lista:
        if x%2==0:
            par.append(x)
        else:
            impar.append(x)
    print(par,impar)

ordenarLista([5,2,4,7,9,1,8])

## FUNCION FACTORIAL
from math import factorial
def factorial(numero):
    if numero>0:
        print(factorial(numero))
    else:
        print("error no es numero positivo")

factorial(5)
factorial(-1)

def asignacion():
    return 1,2,3,4,5

a,b,c,d,e = asignacion()
print(a,c)