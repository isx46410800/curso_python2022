#!/usr/bin/python3

# importamos la liberia
import pandas as pd
import numpy as np

# SELECCIONAR DATOS
# en series
lista_valores = np.arange(3) # 0 1 2
lista_indices = ["i1","i2","i3"]

serie = pd.Series(lista_valores,index=lista_indices)
print(serie)
# multiplica los datos
serie = serie *2
print(serie)
# seleccionar datos
print(serie["i1"])
print(serie[1])
print(serie[0:2])
print(serie[serie>2])
serie[serie>2] = 6
print(serie)

# en datafranes
lista_valores2 = np.arange(25).reshape(5,5)
lista_indices2 = ["i1","i2","i3","i4","i5"]
lista_columnas2 = ["c1","c2","c3","c4","c5"]

dataframe = pd.DataFrame(lista_valores2,index=lista_indices2,columns=lista_columnas2)
print(dataframe)
print(dataframe["c2"]) # todo lo de la c2
print(dataframe["c2"]["i2"]) # valor concreto
print(dataframe[dataframe["c2"]>15]) # los datos de donde la columnas2
print(dataframe>20) # muestra cada valor si es true o false
print(dataframe.loc["i2"]) # fila i2