#!/usr/bin/python3

import pandas as pd

# TRATAMIENTO DE DATOS EN URL HTML
url = "https://es.wikipedia.org/wiki/Anexo:Finales_de_la_Copa_Mundial_de_F%C3%BAtbol"

# cogemos los datos de la url
dataframe_futbol = pd.io.html.read_html(url)


# cambiamos los campos de columnas por los que queremos
print(dataframe_futbol.loc[0])
# creamos un diccionario con los nombres de las columnas
diccionario = dict(dataframe_futbol.loc[0])
print(diccionario)
# lo asignamos al nuevo dataframe
dataframe_futbol = dataframe_futbol.rename(columns=diccionario)
# ahora borramos la fila 1 que se repite y borramos la columna notas
dataframe_futbol = dataframe_futbol.drop(0)
dataframe_futbol = dataframe_futbol.drop("Notas",axis=1)
print(dataframe_futbol)