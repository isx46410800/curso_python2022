#!/usr/bin/python3

import pandas as pd

dataframe1 = pd.DataFrame({"c1" : ["1","2","3"], "clave" : ["a","b","c"]})
dataframe2 = pd.DataFrame({"c2" : ["4","5","6"], "clave" : ["c","b","e"]})

# hacer una union de datafames segun columna. Une por claves iguales
dataframe3 = pd.DataFrame.merge(dataframe1,dataframe2,on="clave")
print(dataframe3)
# une igual pero manteniendo los datos del de la izquierda
dataframe4 = pd.DataFrame.merge(dataframe1,dataframe2,on="clave",how="left")
print(dataframe4)
# une igual pero manteniendo los datos del de la derecha
dataframe5 = pd.DataFrame.merge(dataframe1,dataframe2,on="clave",how="right")
print(dataframe5)
