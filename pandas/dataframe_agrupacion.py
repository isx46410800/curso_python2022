#!/usr/bin/python3

import pandas as pd
import numpy as np

lista_valores = {"clave1": ["x","x","y","y","z"],"clave2": ["a","b","a","b","a"], "datos1": np.random.rand(5), "datos2": np.random.rand(5)}

dataframe = pd.DataFrame(lista_valores)
print(dataframe)
# queremos agrupar datos1 con clave1
agrupacion = dataframe["datos1"].groupby(dataframe["clave1"])
print(agrupacion)
# te hace una media de x y z
print(agrupacion.mean())