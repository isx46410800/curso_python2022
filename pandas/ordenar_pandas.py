#!/usr/bin/python3

# importamos la liberia
import pandas as pd
import numpy as np

# ORDENAR Y CLASIFICAR
lista_valores = range(4)
lista_indices = list("bcad")
lista_columnas = list("wxyz")
serie = pd.Series(lista_valores,index=lista_indices)
print(serie)

print(serie.sort_index())
print(serie.sort_values())
print(serie.rank()) # ordena ppor ranking de clasificaciones
# esto crea una sserie con 10 datos randoms
serie2 = pd.Series(np.random.randn(10))
print(serie2)
print(serie2.rank())

# CLASIFICAR DATOS
lista_indices2 = list("abc")
lista_columnas2 = list("xyz")
dataframe = pd.DataFrame(np.arange(9).reshape(3,3),index=lista_indices2, columns=lista_columnas2)
print(dataframe)
print(dataframe.describe())
print(dataframe.sum())
print(dataframe.max())
print(dataframe.max(axis=1))
print(dataframe.min())
print(dataframe.idxmin())