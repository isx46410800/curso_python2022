#!/usr/bin/python3

# importamos la liberia
import pandas as pd
import numpy as np

# ELIMINAR DATOS

## INDICES
print(np.arange(4))
# creamos una serie a partir de un array de numpy y ponemos los indices
serie = pd.Series(np.arange(4), index=["a","b","c","d"])
print(serie)

# eliminamos indices
serie.drop("c")

## DATAFRAMES
print(np.arange(9).reshape(3,3))
lista_valores = np.arange(9).reshape(3,3)
lista_indices = ["a","b","c"]
lista_columnas = ["c1","c2","c3"]

dataframe = pd.DataFrame(lista_valores, index=lista_indices,columns=lista_columnas)
print(dataframe)
dataframe.drop("b")
dataframe.drop("c2", axis=1)
