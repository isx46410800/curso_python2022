#!/usr/bin/python3

# importamos la liberia
import pandas as pd
import numpy as np

# VALORES NULOS
lista_valores = [1,2,np.nan,4]
lista_indices = list("abcd")

serie = pd.Series(lista_valores,index=lista_indices)
print(serie)
print(serie.isnull()) # ver booleanos los nulos
print(serie.dropna()) # borrar los nulos

lista_valores2 = [[1,2,1],[1,np.nan,2],[1,2,np.nan]]
lista_indices2 = list("123")
lista_columnas2 = list("abc")
dataframe = pd.DataFrame(lista_valores2,index=lista_indices2,columns=lista_columnas2)
print(dataframe)
print(dataframe.isnull())
print(dataframe.dropna())
print(dataframe.fillna(0)) # rellena los nulos con 0