#!/usr/bin/python3

import pandas as pd
import numpy as np

lista_valores = np.arange(25).reshape(5,5)

dataframe = pd.DataFrame(lista_valores)

# cambiamos los indices por una combinacion aleatoria
combinacion = np.random.permutation(5)
print(combinacion)

# ahora ordena los valores segun el indice de la combinacion
print(dataframe.take(combinacion))