#!/usr/bin/python3

import pandas as pd
import numpy as np

lista_valores = [[1,2,3],[4,5,6],[7,8,9],[np.nan,np.nan,np.nan]]
lista_columnas = list("abc")
dataframe = pd.DataFrame(lista_valores,columns=lista_columnas)
print(dataframe)

# agregaciones de suma, minimo sobre los datos
print(dataframe.agg(["sum","min"]))
# por fila
print(dataframe.agg("sum",axis=1))
