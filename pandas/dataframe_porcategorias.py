#!/usr/bin/python3

import pandas as pd
import numpy as np

precio = [42,50,45,23,5,21,88,34,26]
rango = [10,20,30,40,50,60,70,80,90,100]

# AGRUPAR CATEGORIAS
# te sale en que rango está cada precio de la lista de rangos
precio_con_rango = pd.cut(precio,rango)
print(precio_con_rango)
# cuenta cuantos hay para cada rango de precios
print(pd.value_counts(precio_con_rango))
