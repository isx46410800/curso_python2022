#!/usr/bin/python3

import pandas as pd
import numpy as np

lista_valores = np.arange(9).reshape(3,3)
lista_indices = list("abc")
lista_columnas = ["columna1","columna2","columna3"]

dataframe = pd.DataFrame(lista_valores, index=lista_indices, columns=lista_columnas)
print(dataframe)
# REEMPLAZAMOS LOS INDICES
# POR MAYUSCULAS
nuevos_indices = dataframe.index.map(str.upper)
dataframe.index = nuevos_indices
print(dataframe)
# POR RENAME
dataframe = dataframe.rename(index=str.lower)
print(dataframe)
# NUEVOS INDICES
indices_nuevos = {"a":"f","b":"w","c":"z"}
dataframe = dataframe.rename(index=indices_nuevos)
print(dataframe)
# O SOLO UNO DE LOS QUE HAY
indices_uno = {"f":"xxx"}
dataframe = dataframe.rename(index=indices_uno)
print(dataframe)