#!/usr/bin/python3

import pandas as pd

# TRATAMIENTO DE DATOS EN FILE EXCELL

# cogemos los datos deL FICHERO
file_excell = pd.ExcelFile("/home/miguel/Documents/curso_python2022/pandas/poblacion.xlsx")
# en csv seria:
# file_csv = pd.read_csv("/home/miguel/Documents/curso_python2022/pandas/poblacion.xlsx")

# parseamos en que hoja está
dataframe = file_excell.parse("Hoja1")

print(dataframe)
