#!/usr/bin/python3

import pandas as pd
import numpy as np

# 10 filas por 3 columnas
lista_valores = np.random.rand(10,3)
# creamos un dataframe
dataframe = pd.DataFrame(lista_valores)
print(dataframe)
# filtramos por columna y valor
columna0 = dataframe[0]
print(columna0)
print(columna0[columna0>0.40])
# tmb por dataframe se puede
print(dataframe[dataframe>0.40])
