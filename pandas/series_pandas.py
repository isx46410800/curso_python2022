#!/usr/bin/python3

# importamos la liberia
from logging.config import dictConfig
import pandas as pd

## INDICES

# te crea una serie de indices #0 1
serie1 = pd.Series([1,5,7])    #1 5
print(serie1)
# ver un indice
print(serie1[1]) #5
# crear un indice
asignaturas = ["mates", "sociales", "fisica"]
notas = [8,9,5]
serie_notas_miguel = pd.Series(notas,index=asignaturas)
print(serie_notas_miguel)
print(serie_notas_miguel["mates"])
#condiciones
print(serie_notas_miguel[serie_notas_miguel>7])
#asignamos nombres
serie_notas_miguel.name = "Notas de Miguel"
serie_notas_miguel.index.name = "Asignaturas de Bachiller"
print(serie_notas_miguel)
# convertir la serie en un diccionario
diccionario = serie_notas_miguel.to_dict()
print(diccionario)
serie = pd.Series(diccionario)
print(serie)
#operaciones con indices con las series
notas2 = [1,5,3]
serie_notas_natalia = pd.Series(notas2,index=asignaturas)
print(serie_notas_natalia)
notas_medias = (serie_notas_miguel+serie_notas_natalia)/2
print(notas_medias)