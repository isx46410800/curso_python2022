#!/usr/bin/python3

import pandas as pd
import numpy as np


lista_valores = [[1,2],[1,2],[5,6],[5,8]]
lista_indices = list("mnop")
lista_columnas = ["columna1","columna2"]

dataframe = pd.DataFrame(lista_valores, index=lista_indices, columns=lista_columnas)
print(dataframe)

# BORRAR LOS DUPLICADOS
dataframe_duplicado = dataframe.drop_duplicates()
print(dataframe_duplicado)

# BORRAR LOS DUPLICADOS PARA SOLO VALORES UNICOS EN UNA COLUMNA Y MANTENER EL ULTIMO VALOR
dataframe_duplicado_columna = dataframe_duplicado.drop_duplicates(["columna1"], keep="last")
print(dataframe_duplicado_columna)