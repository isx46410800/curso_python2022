#!/usr/bin/python3

# importamos la liberia
import pandas as pd
import webbrowser # sirve para paginas web
## DATAFRAMES
# sirve para abrir esta web
website = "https://es.wikipedia.org/wiki/Anexo:Campeones_de_la_NBA"
webbrowser.open(website)
# copiamos algo al portapapeles de esa web y lo asignamos a un dataframe
dataframe_nba = pd.read_clipboard()
print(dataframe_nba)
# te dice el nombre de las columnas
print(dataframe_nba.columns)
print(dataframe_nba.columns["Campeón del Oeste"])
# ver la info por el indice
print(dataframe_nba.loc[5])
# ver los 5 primeros con head o especificos
print(dataframe_nba.head())
print(dataframe_nba.head(7))
print(dataframe_nba.tail())
print(dataframe_nba.tail(4))

# dataframe con un diccionario
asignaturas = ["Mates", "Reli", "Sociales", "Natus"]
notas = [8,5,7,8]
diccionario = { "Asignaturas": asignaturas, "Notas": notas}
print(diccionario)
# a raiz del diccionario te crea un dataframe con los nombres, notas e indices
dataframe_notas = pd.DataFrame(diccionario)
print(dataframe_notas)