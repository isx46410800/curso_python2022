#!/usr/bin/python3

import pandas as pd
import numpy as np

# concatenamos los dos arrays
array1 = np.arange(9).reshape(3,3)
array_concatenado = np.concatenate([array1,array1])
print(array_concatenado)

# concatenamos series poniendo los campos de serie
serie1 = pd.Series([1,2,3], index=["a","b","c"])
serie2 = pd.Series([4,5,6], index=["d","e","f"])
serie_concatenada = pd.concat([serie1,serie2], keys=["serie1","serie2"])
print(serie_concatenada)

# concatenamos datafranes
dataframe1 = pd.DataFrame(np.random.rand(3,3), columns=["a","b","c"]) #3 filas 3 columnas
dataframe2 = pd.DataFrame(np.random.rand(2,3), columns=["a","b","c"])
dataframe3 = pd.concat([dataframe1,dataframe2],keys=["dataframe1","dataframe2"])
print(dataframe3)

# combinar serie y dataframes
serie_combinada = serie1.combine_first(serie2)
print(serie_combinada)
dataframe_combinado = dataframe1.combine_first(dataframe2)
print(dataframe_combinado)