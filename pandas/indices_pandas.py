#!/usr/bin/python3

# importamos la liberia
import pandas as pd

# INDICES
lista_valores = [1,2,3]
lista_indices = ["a", "b", "c"]

serie = pd.Series(lista_valores,index=lista_indices)
print(serie) # a 1 b 2 c 3
print(serie.index) # a b c
print(serie.index[0]) # a

lista_valores2 = [[1,2,3], [8,9,10], [3,5,6]]
lista_indices2 = ["Mates", "Natus", "Fisica"]
lista_nombres = ["Miguel","Natalia", "Cristina"]
# creamos el dataframe indicando indices, columnas y datos
dataframe = pd.DataFrame(lista_valores2, index=lista_indices2, columns=lista_nombres)
print(dataframe)
print(dataframe.index)
print(dataframe.index[2])