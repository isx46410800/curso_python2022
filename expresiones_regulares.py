#!/usr/bin/python3

# importamos el modulo de expresiones regulares
import re

texto = "Hola me llamo miguel"

# SEARCH
# buscamos ese patron dentro del texto
resultado = re.search("llamo",texto)
# que acabe en miguel$, que empiece en ^miguel 
# que haya algo entre me.*miguel

if resultado:
    print("OK")
else:
    print("No encontado")

# FINDALL
texto2 = '''
el coche de miguel es rojo
el coche de natalia es blanco
'''
resultado = re.findall("coche.*rojo", texto2)

if resultado:
    print("OK")
else:
    print("No encontado")

# SPLIT
texto3 = "La silla es blanca y vale 80"
resultado = re.split("\s", texto3)

if resultado:
    print(resultado)
else:
    print("No encontado")

# SUB
texto4 = "La silla es blanca y vale 80"
resultado = re.sub("blanca", "roja", texto4)

if resultado:
    print(resultado)
else:
    print("No encontado")