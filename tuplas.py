#!/usr/bin/python3

## CREACION CONJUNTOS
conjunto = {1,2,3,4,5}
print(conjunto)
print(type(conjunto))

## añadir
conjunto.add(7)
print(conjunto)
conjunto.update([7,8,9])
print(conjunto)

## borrar
conjunto.remove(4)
print(conjunto)
conjunto.discard(7)
print(conjunto)
conjunto.pop()
print(conjunto)
conjunto.clear()


## CREACION TUPLAS (no se puede modificar valores)
tupla = (1,5,6,7)
print(tupla)
print(type(tupla))
print(tupla[2])

## ejercicio 1 
tuplaMeses = ('enero','febrero','marzo','abril','mayo','junio','julio','agosto','septiembre','octubre','noviembre','diciembre')

mes = int(input("Dime numero de mes: "))

print(f'tu mes es {tuplaMeses[mes-1]}')
print("tu siguiente mes es {}".format(tuplaMeses[mes]))