#!/usr/bin/python3

# DOCTEST PARA PODER HACER PRUEBAS EN EL DOCSTRING
def sumar(num1,num2):
    """
    Esto es una funcion para sumar numeros
    >>> sumar(4,3)
    7
    >>> sumar(4,3)
    8
    >>> sumar(2,3)
    5
    """
    return num1+num2

print(sumar(2,2))

# se importa al final la libreria para hacer las pruebas
import doctest
doctest.testmod()