#!/usr/bin/python3

# creamos un fichero y metodo write y texto para que escriba
fichero = open("./texto2.txt", "wt")

# escribir los datos
texto = "Esto es un ejemplo para\nescribir en un fichero\nque vamos a grabar.\n"
fichero.write(texto)

# cerramos dichero
fichero.close()

# al ejecutarlo se nos crea un fichero con ese nombre y ese texto.