#!/usr/bin/python3

# importamos el modulo para ficheros binarios
import pickle

# añadimos a un fichero una lista de colores en modo binarios
colores = ["azul", "amarillo", "verde"]
fichero = open("binarios_colores.pckl", "wb")

pickle.dump(colores, fichero)
fichero.close()