#!/usr/bin/python3

# importamos el modulo
import ficheros_modulo

# creamos nombre del fichero
nombre_fichero = "fichero1.txt"
fichero = ficheros_modulo.Fichero(nombre_fichero)
# en el fichero que vamos a crear, tiene este texto
texto = "Esta es la linea para añadir.\nEsta la segunda linea.\n"
fichero.grabar_fichero(texto)
# añadimos al fichero este texto
texto = "Esta es la linea para añadir.\n"
fichero.incluir_fichero(texto)
# leemos lo que tenemos de fichero
leer_texto = fichero.leer_fichero()
print(leer_texto)
