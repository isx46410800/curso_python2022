#!/usr/bin/python3

# leer un fichero en modo read y texto y lo guardamos en una variable
fichero = open("./texto.txt", "rt")

# ver los datos
datos_fichero = fichero.read()
print(datos_fichero)