#!/usr/bin/python3

# creamos un fichero y metodo append y texto para que añada cosas al file
fichero = open("./texto3.txt", "at")

# escribir los datos
texto = "\nEsto es un ejemplo para\nañadir texto\n"
fichero.write(texto)

# cerramos dichero
fichero.close()

# al ejecutarlo se nos crea un fichero con ese nombre y ese texto.