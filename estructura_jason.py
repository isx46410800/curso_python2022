#!/usr/bin/python3

# importamos el modulo de json
import _json

producto1 = {"nombre":"miguel", "apellido":"amoros"}

estructura_json = _json.dumps(producto1)
print(estructura_json)

producto2 = _json.loads(estructura_json)
print(producto2)