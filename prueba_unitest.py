#!/usr/bin/python3

# DOCTEST PARA PODER HACER PRUEBAS EN EL DOCSTRING
def sumar(num1,num2):
    """
    Esto es una funcion para sumar numeros
    """
    return num1+num2

print(sumar(2,2))

# se importa al final la libreria para hacer las pruebas
import unittest
class pruebas(unittest.TestCase):
    def test(self):
        self.assertEqual(sumar(4,5),9)
        self.assertEqual(sumar(4,5),19)

# prueba
if __name__ == '__main__':
    unittest.main()