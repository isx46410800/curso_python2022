#!/usr/bin/python3

import random
import math
# CALCULO DE BOTES DE PINTA PARA PINTAR UNA PARED

# alto de la pared 
# ancho de la pared 
# cuantos metros cuadrados cubre un bote de pintura 
# calculo de (alto*ancho)/m2 que cubre cada bote 


print("Bienvenidos al calculo de botes de pintura")

# CON FUNCION
def calculo_pared(alto,ancho,superficie):
    botes = (alto*ancho)/superficie
    # redondea hacia arriba
    numero_botes = math.ceil(botes)
    return numero_botes

print(calculo_pared(3,4.5,1))

alto = float(input("Escribe cuanto tiene de alto la pared: "))
ancho = float(input("Escribe cuantos tiene de ancho la pared: "))
superficie = float(input("Escribe cuantos metros cuadrados cubre un bote de pintura: "))

# botes = (alto*ancho)/superficie
numero_botes = calculo_pared(alto,ancho,superficie)
print(f"Necesitas {numero_botes} botes de pintura para tu pared")

