#!/usr/bin/python3

# librerias
import random
import replit

# PROGRAMA BLACKJACK
## llegar a 21

# FUNCIONES

def generar_carta():
    cartas = [11,2,3,4,5,6,7,8,9,10,10,10]
    carta = random.choice(cartas)
    return carta

print(generar_carta())

def calcular_suma(cartas):
    # sum suma los valores de la lista
    if sum(cartas) == 21 and len(cartas) == 2:
        return 0
    if 11 in cartas and sum(cartas) > 21:
        cartas.remove(11)
        cartas.append(1)
    return sum(cartas)

print(calcular_suma([4,5,6]))

def mostrar_ganador(marcador_usuario,marcador_ordenador):
    if marcador_usuario == marcador_ordenador:
        texto = "Empate"
    elif marcador_ordenador == 0:
        texto = "Has perdido, el PC tiene 21 - Blackjack"
    elif marcador_usuario == 0:
        texto = "Has ganado, tienes 21 - Blackjack"
    elif marcador_usuario > 21:
        texto = "Has perdido, tus cartas suman mas de 21"
    elif marcador_ordenador > 21:
        texto = "Has ganado, las cartas del PC suman mas de 21"
    elif marcador_usuario > marcador_ordenador:
        texto = "Has ganado!"
    else:
        texto = "Has perdido"
    return texto
    
print(mostrar_ganador(21,20))

def jugar():
    print("Estamos jugando...")
    cartas_usuario = []
    cartas_ordenador = []
    finalizado = False
    # hacemos el reparto de dos cartas para jugar
    for reparto in range(2):
        carta = generar_carta()
        cartas_usuario.append(carta)
        carta2 = generar_carta()
        cartas_ordenador.append(carta2)
    print(f"Cartas usuario: {cartas_usuario}")
    print(f"Cartas pc: {cartas_ordenador}")

    while not finalizado:
        marcador_usuario = calcular_suma(cartas_usuario)
        marcador_ordenador = calcular_suma(cartas_ordenador)
        print(f"Cartas usuario: {marcador_usuario}")
        print(f"Cartas pc: {marcador_ordenador}")
        # miramos resultados para finalizar juego
        if marcador_usuario == 0 or marcador_ordenador == 0 or marcador_usuario>21:
            finalizado = True
        else:
            mas_cartas = input("Quieres mas cartas? Escribe si o no: ")
            if mas_cartas == 'si':
                carta = generar_carta()
                cartas_usuario.append(carta)
            else:
                finalizado = True
    while marcador_ordenador !=0 and marcador_ordenador < 17:
        carta_extra = generar_carta()
        cartas_ordenador.append(carta_extra)
        marcador_ordenador = calcular_suma(cartas_ordenador)
    print(f"Cartas usuario: {cartas_usuario}")
    print(f"Cartas pc: {cartas_ordenador}")
    print(f"Cartas usuario: {marcador_usuario}")
    print(f"Cartas pc: {marcador_ordenador}")
    # mostramos ganador
    ganador = mostrar_ganador(marcador_usuario,marcador_ordenador)
    print(ganador)

# Programa
while input("¿Quieres jugar al Blacjack? Escribre 'si' o 'no': ").lower() == 'si':
    replit.clear()
    jugar()
