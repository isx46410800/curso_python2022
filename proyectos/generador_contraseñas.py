#!/usr/bin/python3

import random
# GENERADOR DE CONTRASEÑAS

# Pedir cuantas letras, numeros y simbolos necesitas

letras = ["A","B","C","D","E"]
numeros = ["1","2","3","4","5"]
simbolos = ["!","#","@","$","&"]

print("Bienvenidos al generador de contraseñas")
numero_letras = int(input("Escribe cuantas letras quieres: "))
numero_numeros = int(input("Escribe cuantos numeros quieres: "))
numero_simbolos = int(input("Escribe cuantos simbolos quieres: "))

lista = []

# por cada caracter que quiera, lo añadimos a la lista 
for letra in range(1, numero_letras+1):
    lista.append(random.choice(letras))

for letra in range(1, numero_numeros+1):
    lista.append(random.choice(numeros))

for letra in range(1, numero_simbolos+1):
    lista.append(random.choice(simbolos))   

# resultado de la lista
print(lista)

# ponemos la lista aleatoria
random.shuffle(lista)
print(lista)

# creamos la contraseña
password = ""
for caracter in lista:
    password = password + caracter

print(password)