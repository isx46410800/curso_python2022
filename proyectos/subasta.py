#!/usr/bin/python3

# GESTIONAR UNA SUBASTA

# preguntar nombre y precio apuesta
# añadirlo a un diccionario
# borrar la pantalla
# preguntar si hay mas personas para esta apuesta (bucle hasta no)
# SI: repetir misma preguntas y añadir
# NO: mostrar la apuesta ganadora y fin del programa

# libreria para limpiar la pantalla
import os 

print("Bienvenidos a la SUBASTA de MIGUEL")

# creamos el diccionario
diccionario = {}

# funcionar calcular el ganador
def calcular_ganador(diccionario):
    max = 0
    ganador = ""
    for nombre in diccionario:
        precio = diccionario[nombre]
        if precio > max:
            max = precio
            ganador = nombre
    print(f"El ganador es {ganador} con apuesta de {max}")

# bucle para que mientras haya mas apuestas, siga preguntando y añadiendo
mas_subastas = True
while mas_subastas:
    precio = float(input("Escribe tu apuesta: "))
    nombre = input("Escribe nombre: ")
    diccionario[nombre]=precio

    pregunta = input("Hay mas apuesta? Escribe 'si' o 'no'").lower()
    if pregunta == "si":
        os.system("clear")
    elif pregunta == "no":
        mas_subastas = False
    else:
        print("Eleccion incorrecta")

# decimos ganador
os.system("clear")
calcular_ganador(diccionario)