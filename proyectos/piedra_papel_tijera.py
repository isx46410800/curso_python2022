#!/usr/bin/python3

# JUEGO DE PIEDRA PAPEL Y TIJERA

# El usuario elije una opcion
# el ordenador una aleatoria

# resultado
# tijera gana a papel
# papel gana a piedra
# piedra gana a tijera

from random import randrange


print("Bienvenidos al juego de piedra, papel y tijera")
usuario = int(input("Escribe '0' piedra, '1' papel o '2' tijera: "))
if usuario not in range(0,3):
    print("Numero no valido, vuelve a intentarlo..!")
else:
    ordenador = randrange(0,3)

print("El usuario ha elegido: ", usuario)
print("El ordenador ha elegido: ", ordenador)

if (usuario > ordenador) or (usuario == 0 and ordenador == 2):
    print("El usuario ha ganado!!")
elif usuario == ordenador:
    print("ui...empate!")
else:
    print("El ordenador ha ganado, has perdido...!!")