#!/usr/bin/python3

# JUEGO DE PREGUNTAS

print("Bienvenidos al juego de las preguntas")
respuesta1 = input("Quieres ir a la izquierda o derecha. Escribe 'izquieda' o 'derecha'. ").lower()

if respuesta1 == "izquierda":
    print("Respuesta fallida, has perdido. Vuelvelo a intentar")
elif respuesta1 != "derecha":
    print("Has fallado, respuesta introducida invalida")
else:
    respuesta2 = input("Quieres ir a nadando o caminar. Escribe 'nadar' o 'caminar'. ").lower()
    if respuesta2 == "nadar":
        print("Respuesta fallida, has perdido. Vuelvelo a intentar")
    elif respuesta1 != "caminar":
        print("Has fallado, respuesta introducida invalida")
    else:
        respuesta3 = input("Has llegado a una casa con tres puertas. Escribe 'rojo', 'verde' o 'azul'. ").lower()
        if respuesta3 == "rojo" or respuesta3 == "verde":
            print("Respuesta fallida, has perdido. Vuelvelo a intentar")
        elif respuesta1 != "azul":
            print("Has fallado, respuesta introducida invalida")
        else:
            print("Enhorabuena, has ganado el juego!")