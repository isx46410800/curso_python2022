#!/usr/bin/python3

import sqlite3

# funcion para conectar y crear bbdd y su tabla
def conectar():
    conexion = sqlite3.connect("libros.db")
    cursor = conexion.cursor()
    cursor.execute("CREATE TABLE IF NOT EXISTS libros(id INTEGER PRIMARY KEY, titulo TEXT, autor TEXT, year INTEGER, isbn INTEGER)")
    conexion.commit()
    conexion.close()


# funcionar para insertar datos
def insertar(titulo,autor,year,isbn):
    conexion = sqlite3.connect("libros.db")
    cursor = conexion.cursor()
    cursor.execute("INSERT INTO libros VALUES(NULL,?,?,?,?)",(titulo,autor,year,isbn))
    conexion.commit()
    conexion.close()

# visualizar los parametros
def visualizar():
    conexion = sqlite3.connect("libros.db")
    cursor = conexion.cursor()
    cursor.execute("SELECT * FROM libros")
    consulta = cursor.fetchall()
    conexion.close()
    return consulta

# funcion para buscar libro
def buscar(titulo="",autor="",year=0,isbn=0):
    conexion = sqlite3.connect("libros.db")
    cursor = conexion.cursor()
    cursor.execute("SELECT * FROM libros WHERE titulo=? OR autor=? OR year=? OR isbn=?",(titulo,autor,year,isbn))
    consulta = cursor.fetchall()
    conexion.close()
    return consulta

# borrar un libro
def borrar(id):
    conexion = sqlite3.connect("libros.db")
    cursor = conexion.cursor()
    cursor.execute("DELETE FROM libros WHERE id=?",(id,))
    conexion.commit()
    conexion.close()

# actualizar un libro
def actualizar(titulo,autor,year,isbn,id):
    conexion = sqlite3.connect("libros.db")
    cursor = conexion.cursor()
    cursor.execute("UPDATE libros SET titulo=?, autor=?, year=?, isbn=? WHERE id=?",(titulo,autor,year,isbn,id))
    conexion.commit()
    conexion.close()
    


# PRUEBAS
conectar()
# insertar("titulo1","autor1",2001,1111111111)
# insertar("titulo2","autor2",2002,2222222222)
# insertar("titulo3","autor3",2003,3333333333)
print("VISUALIZAR")
consultas = visualizar()
for consulta in consultas:
    print(consulta)
print("BUSQUEDA POR TITULO")
busqueda = buscar(titulo="titulo1")
for x in busqueda:
    print(x)
print("BORRAR POR TITULO")
#borrar(3)
print("VISUALIZAR")
consultas = visualizar()
for consulta in consultas:
    print(consulta)
print("ACTUALIZAR UN LIBRO")
actualizar(titulo="titulo5",autor="autor5",year=2005,isbn=5555555,id=2)
consultas = visualizar()
for consulta in consultas:
    print(consulta)


