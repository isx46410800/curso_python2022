#!/usr/bin/python3

# JUEGO PARA CALCULADORA DE PROPINAS
# precio de la factura
# porcentaje de propina
# personas a repartir

print("Bienvenido a la calculadora de propinas")
factura = float(input("precio de la factura?"))
propina = int(input("Cual es el porcentaje de propina a dejar?"))
personas = int(input("Entre cuantas personas hay que repatir la factura?"))

importe_propina = (factura*propina)/100
factura_total = round(factura + importe_propina,2)
reparto = factura_total/personas

print("El importe por persona es: ", reparto)
