#!/usr/bin/python3

# libreria para test automatizados
import unittest
from analisis_datosSQL import *

# fichero a testear
nombre_fichero = "coches.csv"

# creamos la prueba del numero total de coches que hay en la tabla
class test_numero_coches_tabla(unittest.TestCase):
    def test(self):
        borrar_datos()
        datos = leer_datos(nombre_fichero)
        # creamos bbdd y nos conectamos
        conexion = crear_conexion_bd()
        # creamos la tabla
        crear_tabla(conexion)
        # creamos el contenido de la tabla
        crear_informacion(conexion,datos)
        # consulta de numero de coches
        numero = consulta_coches2(conexion)
        self.assertEqual(2780,numero)

class test_numero_precio_total(unittest.TestCase):
    def test2(self):
        borrar_datos()
        datos = leer_datos(nombre_fichero)
        # creamos bbdd y nos conectamos
        conexion = crear_conexion_bd()
        # creamos la tabla
        crear_tabla(conexion)
        # creamos el contenido de la tabla
        crear_informacion(conexion,datos)
        # consulta de numero de coches
        precio = consulta_coches3(conexion)
        self.assertEqual(38997136.0,precio)


# ejecutar en modo principal
if __name__ == '__main__':
    unittest.main()