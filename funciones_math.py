#!/usr/bin/python3

# CREAR FUNCIONES MATHS
import math

## hace una subida exponencial `pow(5,2)==5**2`
print(math.pow(4,3))

## raiz cuadrada y raiz cuadrada solo entero
print(math.sqrt(16))
print(math.isqrt(17))

## factorial
print(math.factorial(5))

## tangente, coseno y seno
print(math.sin(90))
print(math.cos(90))
print(math.tan(90))

## numero random
import random
print(random.randint(0,100))

## funcion comparar numeros
def comparacion(num1,num2):
    if num1 > num2:
        return 1
    elif num1 == num2:
        return 0
    else:
        return -1

print(comparacion(5,2))
print(comparacion(-5,2))
print(comparacion(2,2))


## funcion calcular +IVA
def iva(importe,iva):
    iva = iva / 100
    return importe + importe*iva

print(iva(1000,21))

## area circulo y cuadrado
def areaCuadrado(base,altura):
    return base*altura

def areaCirculo(radio):
    return (math.pi*(radio*radio))

print(areaCuadrado(2,5))
print(areaCirculo(5))

## lista y devuelve la media
def average(lista):
    return sum(lista)/len(lista)

print(average([1,2,3,4,10]))

