#!/usr/bin/python3

# GESTION DE ERRORES  

# MENSAJE BASICO
edad = int(input("Dime tu edad: "))
print("Tu edad es:", edad)

# TRY-EXCEPT: intento y notificacion del error
try:
    edad = int(input("Dime tu edad: "))
    print("Tu edad es:", edad)
except:
    print("Edad ingresada incorrectamente")

# WHILE: BUCLE PARA VOLVER A PREGUNTAR
while True:
    try:
        edad = int(input("Dime tu edad: "))
        print("Tu edad es:", edad)
        break
    except:
        print("Edad ingresada incorrectamente")

# FINALLY: siempre se ejecuta este mensaje
while True:
    try:
        edad = int(input("Dime tu edad: "))
        print("Tu edad es:", edad)
        break
    except:
        print("Edad ingresada incorrectamente")
    finally:
        print("codigo ejecutado correctamente")

# MULTIPLES EXCEPT
while True:
    try:
        edad = int(input("Dime tu edad: "))
        print("Tu edad es:", edad)
        break
    except ZeroDivisionError:
        print("No se puede dividir entre zero")
    except ValueError:
        print("Numero incorrecto")
    except KeyboardInterrupt:
        print("Has cancelado la ejecucion")
        break
    finally:
        print("codigo ejecutado correctamente")