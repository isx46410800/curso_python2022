#!/usr/bin/python3

# importamos el modulo de bd sqlite
import sqlite3  

# creamos/existente bbdd
conexion = sqlite3.connect("bbdd1.db")

# sirve para poder hacer sentencias sql dentro
cursor = conexion.cursor()

# creamos una tabla
cursor.execute("CREATE TABLE PERSONAS (nombre TEXT, apellido1 TEXT, apellido2 TEXT, edad INTEGER)")
# creamos fila
cursor.execute("INSERT INTO PERSONAS VALUES ('Antonio', 'Perez', 'Gomez', 35)")
# creamos varias filas
lista_personas = [('Miguel', 'Amoros', 'Moret', 28), ('Natalia', 'Sendra', 'Soler', 26)]
cursor.executemany("INSERT INTO PERSONAS VALUES (?,?,?,?)", lista_personas)

# consulta de datos
cursor.execute("SELECT * FROM PERSONAS")
personas = cursor.fetchall()
for persona in personas:
    print(persona)

# consulta datos con WHERE
cursor.execute("SELECT * FROM PERSONAS WHERE edad > 28")
personas_edad = cursor.fetchall()
for persona in personas_edad:
    print(persona)

# consulta datos con WHERE y ordenado
cursor.execute("SELECT * FROM PERSONAS WHERE edad <= 28 ORDER BY edad DESC")
personas_edad = cursor.fetchall()
for persona in personas_edad:
    print(persona)

# borrar datos
cursor.execute("DELETE FROM PERSONAS WHERE nombre = 'Antonio'")

# actualizar datos
cursor.execute("UPDATE PERSONAS SET nombre = 'Miguelito' where edad = 28")


# mantener el registro guardado
conexion.commit()

# cerramos bdd
conexion.close()
