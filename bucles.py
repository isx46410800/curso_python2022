#!/usr/bin/python3

# BUCLE WHILE  
print("### BUCLE WHILE ###")
i=0

while i<=10:
    print(f'posicion {i}')
    i+=1

## ejercicio 1
numero = int(input("Dime un numero: "))
divisor=0

while divisor <=10:
    print(f'{numero} x {divisor} = {numero*divisor}')
    divisor+=1

## ejercicio 2
edad = int(input("Dime una edad: "))
count = 1

while count<=edad:
    print(count)
    count+=1

# BUCLE FOR
print("### BUCLE FOR ###")
lista = [1,2,3,4]
tupla = (5,6,7,8)

for i in lista:
    print(i)

for j in tupla:
    print(j)

for x in range(0,11,2):
    print(x)

## el break sale del bucle
for x in range(1,10):
    if x==6:
        break
    print(x)
## continue omite esa condicion y continua el bucle.
for x in range(1,10):
    if x==6:
        continue
    print(x)

## Ejercicio 1
num1 = int(input("Dime un numero: "))
num2 = int(input("Dime un numero: "))


for x in range(num1,num2,2):
    if num1%2==0:
        print(x+1)
    else:
        print(x)