#!/usr/bin/python3

# PILARES POO DE OBJETOS  

## ENCAPSULAMIENTO

class A():
    def __init__(self):
        self.contador = 0

    def incrementar(self):
        self.contador += 1

    def cuenta(self):
        return self.contador

class B():
    def __init__(self):
        self.__contador = 0

    def incrementar(self):
        self.__contador += 1

    def cuenta(self):
        return self.__contador

## CON UN SOLO GUION _ATRIBUTO, INDICAMOS A LOS PROGRAMADORES QUE NO LLAMEN CON PRINT, USAR METODO GET
class C():
    def __init__(self):
        self._contador = 0

    def incrementar(self):
        self._contador += 1

    def cuenta(self):
        return self._contador
## creamos el objeto
a = A()
## llamamos a los metodos
print(a.cuenta())
a.incrementar()
print(a.cuenta())
a.cuenta = 20
print(a.cuenta)

## llamamos al atributo
print(a.contador)

## creamos el objeto
b = B()
## llamamos a los metodos
print(b.cuenta())
b.incrementar()
print(b.cuenta())
## llamamos al atributo: ERROR PORQUE __ SOLO SIRVE PARA LLAMARSE DENTRO LA CLASE
print(b.__contador)