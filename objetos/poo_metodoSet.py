class A():
    def __init__(self):
        self._cuenta = 0
        self._contador = 0
    # decorador para metodo get
    @property
    def cuenta(self):
        return self._cuenta
    # decorador para metodo set
    @cuenta.setter
    def cuenta(self , cuenta):
        self._cuenta = cuenta
    
    @property
    def contador(self):
        return self._contador

    @contador.setter
    def contador(self , contador):
        self._contador = contador

a = A()
# al metodo get se le llama solo
# print(a._cuenta)
print(a.cuenta)
# al metodo set se le llama cambiandole el valor
a.cuenta = 20
print(a.cuenta)
print(a.contador)
a.contador = 10
print(a.contador)
# a._cuenta = 10
# print(a._cuenta)
