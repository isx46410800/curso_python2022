#!/usr/bin/python3

## HERENCIA DE OBJETOS Y CLASES
class A():
    def mensaje(self):
        print("Esta es la clase A")
    def primera(self):
        print("Estas dentro de la clase A")

class B():
    def mensaje(self):
        print("Esta es la clase B")
    
    def segunda(self):
        print("Estas dentro de la clase B")

# hereda la B primero y luego A
class C(B,A):
    pass

# creamos objeto
c = C()
c.primera()
c.segunda()