#!/usr/bin/python3

# OBJETOS: TROZO DE CODIGO COM ESTRUCTURAS.
## EJEMPLO: clase es un molde y objeto son galletas, helados,etc
## Los metodos son las funciones de las clases y los atributos las caracteristicas/datos de un objeto.

## SELF quiere decir uno mismo y init se ejecuta el constructor lo primero
# Para crear un objeto se necesita unas clases y dentro sus atributos.

## INIT es el constructor que se activa nada mas empezar y sintetiza sus atributos
class FabricaTelefonos():
    def __init__(self, marca, *colores, **modelos): # *==tupla // **==dict
        self.marca = marca
        self.colores = colores
        self.modelos = modelos

## creamos el objeto con el constructor y sus atributos
# detecta el orden por el orden del metodo y como introducimos los datos
telefono = FabricaTelefonos("Huawei", "Azul", "Negro", "Blanco", m1 = 500, m2 = 300)
print(telefono.marca)
print(telefono.colores)
print(telefono.modelos)

## creamos atributos temporales a ese objeto concreto
telefono.memoria = 512
print(telefono.memoria)
