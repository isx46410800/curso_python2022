#!/usr/bin/python3

# OBJETOS: TROZO DE CODIGO COM ESTRUCTURAS.
## EJEMPLO: clase es un molde y objeto son galletas, helados,etc
## Los metodos son las funciones de las clases y los atributos las caracteristicas/datos de un objeto.

## SELF quiere decir uno mismo y init se ejecuta el constructor lo primero
# Para crear un objeto se necesita unas clases y dentro sus atributos.
class FabricaTelefonos():
    marca = "Huawei"
    
    def elaborarSamsung(self):
        #marca = "Samsung"
        #se pone self para que una vez se asigne aqui, sea para el global ya
        self.marca = "Samsung"

movil = FabricaTelefonos()
print(movil.marca)
movil.elaborarSamsung()
print(movil.marca)

## INIT es el constructor que se activa nada mas empezar y sintetiza sus atributos
class FabricaTelefonos():
    def __init__(self, marca, color):
        self.marca = marca
        self.color = color
    # metodo str para hacer un string
    def __str__(self):
        return f'El objeto tiene atributos {self.marca} y {self.color}'
    # metodo del para borrar de memoria
    def __del__(self):
        print(f'El objeto con {self.marca} y {self.color} borrado')

## creamos el objeto con el constructor y sus atributos
telefono = FabricaTelefonos("Huawei", "Azul")
print(telefono.marca)
print(telefono.color)
print(str(telefono)) ## te crea el string del objeto