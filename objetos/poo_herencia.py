#!/usr/bin/python3

## HERENCIA DE OBJETOS Y CLASES
class Animales():
    def __init__(self,descripcion,especie,hablar):
        self.descripcion = descripcion
        self.especie = especie
        self.hablar = hablar
    
    def habla(self):
        print("Yo hago: ",self.hablar)
    
    def describir(self):
        print("Soy de la especie: ",self.especie)

## HEREDA TODOS LOS METODOS DE LA CLASE ANIMALES
class Perro(Animales):
    pass

## HEREDA TODOS LOS METODOS DE LA CLASE ANIMALES Y AÑADE OTRO
class Abeja(Animales):
    def sonido(self,sonido):
        self.sonido = sonido
        print(self.sonido)

# CREAMOS OBJETOS
## LE PASAMOS LOS ATRIBUTOS DE LA CALSE ANIMALES
perro = Perro("Perro","Mamifero","Guau!")
perro.habla()
perro.describir()
## LE PASAMOS LOS ATRIBUTOS DE LA CALSE ANIMALES
abeja = Abeja("Abeja","Insecto","Brr!")
### le pasamos el nuevo atributo
abeja.sonido("Brr!")
abeja.habla()
abeja.describir()