#!/usr/bin/python3

## HERENCIA DE OBJETOS Y CLASES

class Animales():
    def __init__(self,nombre,mensaje):
        self.nombre = nombre
        self.mensaje = mensaje
    
    def hablar(self):
        print(self.mensaje)
    
class Perro(Animales):
    def hablar(self):
        print("Yo hago Guau!")

class Pez(Animales):
    def hablar(self):
        print("Yo no hablo")

# creamos objeto
perro = Perro("Firulais", "Guau!")
## coge como primero lo de dentro de su funcion y no la general de animales.
perro.hablar()

animales = [Perro("Firulais", "Guau!"),Pez("Nemo", "Nada")]
for i in animales:
    print(i.hablar())