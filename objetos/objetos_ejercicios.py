#!/usr/bin/python3
from math import *

# Ejercicio 1
# Realizar un programa que conste de una clase llamada Alumno que tenga como atributos el nombre y la nota del alumno. Definir los métodos para inicializar sus atributos, imprimirlos y mostrar un mensaje con el resultado de la nota y si ha aprobado o no.
class Alumno():
    def __init__(self,nombre,apellidos,nota):
        self.nombre = nombre
        self.apellidos = apellidos
        self.nota = nota

    def mensaje(self):
        print(f'El alumno {self.nombre} {self.apellidos} ha sacado {self.nota}')
        if self.nota < 5:
            print("Has suspendido")
        else:
            print("Has aprobado")

alumno = Alumno("Miguel", "Amoros Moret", 6)
alumno2 = Alumno("Miguel", "Amoros Moret", 4.9)
alumno.mensaje()
alumno2.mensaje()

# Ejercicio 2
# Escribir una clase en python que calcule pow(x, n) //base,exponente
class Raiz():
    def __init__(self,base,exponente):
        self.base = base
        self.exponente = exponente

    def calculoRaiz(self):
        print(f'El resultado es {pow(self.base, self.exponente)}')

raiz = Raiz(4,2)
raiz.calculoRaiz()

# Ejercicio 3
# Realizar un programa en el cual se declaren dos valores enteros por teclado 
# utilizando el método __init__. 
# Calcular después la suma, resta, multiplicación y división. 
# Utilizar un método para cada una e imprimir los resultados obtenidos. 
# Llamar a la clase Calculadora.
class Calculadora():
    def __init__(self,num1,num2):
        self.num1 = num1
        self.num2 = num2

    def suma(self):
        print(f'El resultado es {self.num1 + self.num2}')
    def resta(self):
        print(f'El resultado es {self.num1 - self.num2}')
    def multiplicacion(self):
        print(f'El resultado es {self.num1 * self.num2}')
    def division(self):
        print(f'El resultado es {self.num1 / self.num2}')

calculadora = Calculadora(5,2)
calculadora.suma()
calculadora.resta()
calculadora.multiplicacion()
calculadora.division()


# Ejercicio 4
# Escribir una clase que se llame Areas(). A partir de un constructor se deben pasar 
# los valores de Base y Altura. 
# Luego, se debe calcular area de un cuadrado, triangulo y pentagono
class Areas():
    def __init__(self):
        self.base = float(input("Dime la base: "))
        self.altura = float(input("Dime la altura: "))

    def triangulo(self):
        print(f'El resultado es {(self.base * self.altura)/2}')
    def cuadrado(self):
        print(f'El resultado es {self.base * self.altura}')
    def pentagono(self):
        print(f'El resultado es {((self.base*5) * self.altura)/2}')

areas = Areas()
areas.triangulo()
areas.cuadrado()
areas.pentagono()

# Ejercicio 5
# Crear una clase Fabrica que tenga los atributos de Llantas, Color y Precio; luego crear dos clases mas que hereden de Fabrica, las cuales son Moto y Carro. Crear metodos que muestren la cantidad de llantas, color y precio de ambos transportes. Por ultimo, crear objetos para cada clase y mostrar por pantalla los atributos de cada uno
class Fabrica():
    def __init__(self, llantas, color, precio):
        self.llantas = llantas
        self.color = color
        self.precio = precio

    def llanta(self):
        print(f'Tiene {self.llantas} llantas')
    def colores(self):
        print(f'Es de color {self.color}')
    def precios(self):
        print(f'Tiene un valor de {self.precio} €')
class Moto(Fabrica):
    pass
class Carro(Fabrica):
    pass

coche = Carro(4,"Rojo", 30000)
coche.llanta()
coche.colores()
coche.precios()
moto = Moto(2,"Azul", 2500)
moto.llanta()
moto.colores()
moto.precios()

# Ejercicio 6

# Crear una clase llamada Marino(), con un metodo que sea hablar, en donde muestre un mensaje que diga "Hola...". Luego, crear una clase Pulpo() que herede Marino, pero modificar el mensaje de hablar por "Soy un Pulpo". Por ultimo, crear una clase Foca(), heredada de Marino, pero que tenga un atributo nuevo llamado mensaje y que muestre ese mesjae como parametro
class Marino():
    def hablar(self):
        print("Hola...")
class Pulpo(Marino):
    def hablar(self):
        print("Soy un Pulpo")
class Foca(Marino):
    def hablar(self, mensaje):
        self.mensaje = mensaje
        print(self.mensaje)

marino = Marino()
marino.hablar()
pulpo = Pulpo()
pulpo.hablar()
foca = Foca()
foca.hablar("holiiiiiiiiiii")

# Ejercicio 7
# Crear un programa con tres clases Universidad, con atributos nombre (Donde se almacena el nombre de la Universidad). Otra llamada Carerra, con los atributos especialidad (En donde me guarda la especialidad de un estudiante). Una ultima llamada Estudiante, que tenga como atributos su nombre y edad. El programa debe imprimir la especialidad, edad, nombre y universidad de dicho estudiante con un objeto llamado persona.
class Universidad():
    def __init__(self, uni):
        self.uni = uni
class Carrera():
    def carrera(self, carrera):
        self.carrera = carrera
class Estudiante(Universidad, Carrera):
    def resultados(self, nombre, apellidos):
        self.nombre = nombre
        self.apellidos = apellidos
        print(f'{self.nombre} {self.apellidos} {self.uni} {self.carrera}')

persona = Estudiante("UAB")
persona.carrera("Informatica")
persona.resultados("Miguel","Amoros")