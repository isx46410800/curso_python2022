#!/usr/bin/python3

# OBJETOS: TROZO DE CODIGO COM ESTRUCTURAS.
## EJEMPLO: clase es un molde y objeto son galletas, helados,etc
## Los metodos son las funciones de las clases y los atributos las caracteristicas/datos de un objeto.

# Para crear un objeto se necesita unas clases y dentro sus atributos.
class FabricaTelefonos():
    marca = "Huawei"
    color = "Azul"
    memoriaRam = 6
    almacenamiento = 128

    #metodos == funciones
    def llamar(self, mensaje):
        return mensaje

    def escucharMusica(self):
        print("Escuchando musiquita!")
    
print(type(FabricaTelefonos))

# Creamos el objeto con la asignacion a las clases.
movil = FabricaTelefonos()
movil.marca
movil.color
movil.memoriaRam
movil.almacenamiento
## llamamos a los atributos de la clase del objeto creadi
print(type(movil))
print(movil)
print(movil.almacenamiento)
## llamamos a los metodos de la clase del objeto creado
print(movil.llamar("Hola bebe, que tal?"))
movil.escucharMusica()
