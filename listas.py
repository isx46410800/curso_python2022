#!/usr/bin/python3

# HACIENDO LISTA
lista = ['miguel', 120, True, 'amoros']

print(lista)
print(type(lista))

# SCILING EN LISTA
print(lista[1])
print(lista[:2])
print(lista[-1])
print(lista[-4:-2])
print(len(lista))  

# MODIFICAMOS LISTA
lista[0] = "Miguel"
print(lista)  

# METODOS LISTA  
lista2 = [1,2,3,4,5,5,5]

## añadir elemento
lista2.append(6)
print(lista2)  
## insertar elemento en lugar concreto
lista2.insert(1, 10)
print(lista2)
## count un elemento en la lista
print(lista2.count(5))
## te dice el lugar de la lista que ocupa een la primera coincidencia
print(lista2.index(5))
## ordenar una lista
lista2.sort()
print(lista2)
## ordenar inversa una lista
lista2.reverse()
print(lista2)
## cambiar valor en una lista
lista3 = ['miguel', 1, 5, 7, 'Amoros', 'Moret']
lista3[2]=555
print(lista3)
## eliminar un valor
lista3.remove(555)
print(lista3)
## eliminar ultimo valor de la lista
lista3.pop()
print(lista3)


# LLENAR LISTAS  
l1 = [1,2,3]
l2 = [4,5,6]
## sumar dos listas
l3 = l1 + l2
print(l3)
l4=[]
for i in l1:
    l4.append(i)
for x in l2:
    l4.append(x)
print(l4)




