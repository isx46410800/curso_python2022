#!/usr/bin/python3

# tkinter - componente raiz
import tkinter
from tkinter import filedialog

# definimos la ventana de la aplicacion
raiz = tkinter.Tk()
raiz.title("Mi programa")

# definimos aspecto de la ventana
frame = tkinter.Frame(raiz)
frame.config(fg="blue",width=400,height=300)
frame.pack()

# construimos un label
texto = "Hola mundo"
label = tkinter.Label(raiz,text=texto)
label.config(fg="green",bg="grey",font=("Cortana",30))
label.pack()

# construimos una entrada por teclado
entrada = tkinter.Entry(raiz)
entrada.config(justify="center", show="*")
entrada.pack()

# construimos una box de texto
box = tkinter.Text(raiz)
box.config(width=20,height=10,font=("Verdana",15),padx=10,pady=10,fg="green",selectbackground="yellow")
box.pack()

# construimos una boton que salte mensaje
def accion():
    print("Hola mundo")

boton = tkinter.Button(raiz,text="Ejecutar",command=accion)
boton.config(fg="green")
boton.pack()

# construimos un multiseleccion
def accion():
    print(f"La opcion es {opcion.get()}")

seleccion1 = tkinter.Radiobutton(raiz,text="Opcion 1",variable=opcion,command=accion)
seleccion1.pack()
seleccion2 = tkinter.Radiobutton(raiz,text="Opcion 2",variable=opcion,command=accion)
seleccion2.pack()

# construimos un checkbutton
def verificar():
    valor = check1.get()
    if valor == 1:
        print("La opcion está activada")
    else:
        print("La opcion está activada")

check1 = tkinter.IntVar()
boton1 = tkinter.Checkbutton(raiz,text="Opcion 1",variable=check1,onvalue=1,offvalue=0,command=verificar)
boton1.pack()

# construimos boton con popup
def avisar():
    tkinter.messagebox.showinfo("Titulo","Mensaje con la info")

boton = tkinter.Button(raiz,text="Pulsar para aviso",command=avisar)
boton.pack()

# construimos boton con popup para responder si o no
def preguntar():
    tkinter.messagebox.askquestion("Titulo","Quieres borrar la info?")

boton = tkinter.Button(raiz,text="Pulsar para preguntar",command=preguntar)
boton.pack()

# construimos boton para añadir fichero
from tkinter import filedialog
def abrirfichero():
    rutafichero = filedialog.askopenfilename(title="Abrir un fichero")
    print(rutafichero)

boton = tkinter.Button(raiz,text="Pulsar para abrir fichero",command=abrirfichero)
boton.pack()

# para que siga ejecutandose
raiz.mainloop()