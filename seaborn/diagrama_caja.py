#!/usr/bin/python3

# importamos la libreria
import pandas as pd
import numpy as np
import matplotlib as mpl
import matplotlib.pyplot as plt
import seaborn as sns

# para crear el grafico en estas pruebas
#%matplotlib inline

datos = np.random.randn(100)

# DIAGRAMA DE CAJA
print(sns.boxplot(datos))