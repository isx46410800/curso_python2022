#!/usr/bin/python3

# importamos la libreria
import matplotlib.pyplot as plt
from turtle import color
import pandas as pd
import numpy as np
import matplotlib as mpl
import seaborn as sns

# para que se cree luego el histograma(en web interactiva~Jupyter)
#print(%matplotlib inline)
datos1 = np.random.randn(100)
print(datos1)

# crea un histograma
# plt.plot(datos1)
# plt.show() 
print(mpl.pyplot.hist(datos1))
print(sns.displot(datos1))

# podemos cambiar el color y oscuridad
print(sns.displot(datos1, color="green"))
print(mpl.pyplot.hist(datos1, color="green", alpha=0.8))

# creamos otros datos
datos2 = np.random.randn(80)
print(sns.displot(datos2, color="yellow"))
print(mpl.pyplot.hist(datos2, color="yellow", alpha=0.8))

# juntamos los dos datos / bin=barras a mostrar
print(mpl.pyplot.hist(datos1, color="yellow", alpha=0.8, bins=20, density=True))
print(mpl.pyplot.hist(datos2, color="blue", alpha=0.8, bins=20, density=True))

# juntamos con seaborn
datos3 = np.random.randn(100)
datos4 = np.random.randn(100)
print(sns.jointplot(datos3,datos4))
print(sns.jointplot(datos3,datos4, kind="hex"))

