#!/usr/bin/python3

# importamos la libreria
import pandas as pd
import numpy as np
import matplotlib as mpl
import matplotlib.pyplot as plt
import seaborn as sns

# para crear el grafico en estas pruebas
#%matplotlib inline

datos = np.random.randn(100)

# DIFERENTES DISTRIBUCIONES DEL HISTOGRAMA
print(sns.distplot(datos))
print(sns.distplot(datos, color="green", hist=False))
# creamos poniendo un color la linea y otro las barras
argumentos_curva = {"color":"black", "label":"Curva"}
argumentos_histograma = {"color":"grey", "label":"Histograma"}
print(sns.distplot(datos, kde_kws=argumentos_curva, hist_kws=argumentos_histograma))

# desde una serie creamos el histograma
serie = pd.Series(datos)
print(sns.distplot(serie, bins=25, color="green"))