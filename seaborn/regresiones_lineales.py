#!/usr/bin/python3

# importamos la libreria
import pandas as pd
import numpy as np
import matplotlib as mpl
import matplotlib.pyplot as plt
import seaborn as sns

# para crear el grafico en estas pruebas
#%matplotlib inline

# regresiones lineales que sirven respecto una variable
# respecto a otras
# nos carga una preguardado de gente dejando propinas
propinas = sns.load_dataset("tips")
print(propinas.head(10))

# creamos un grafico de regresion lineal
# compara la media con las propinas
print(sns.lmplot(x="total_bill",y="tip",data=propinas))
print(sns.lmplot(x="total_bill",y="tip",data=propinas, scatter_kws={"marker": "o","color":"green"},line_kws={"color":"blue"}))

# agreamos otra columna mas a la tabla de propinas
propinas["porcentaje"]= 100*propinas["tip"]/propinas["total_bill"]
print(propinas.head())
# creamos diagrama
print(sns.lmplot(x="size",y="porcentaje",data=propinas))
print(sns.lmplot(x="total_bill",y="porcentaje",data=propinas,hue="sex",markers=["x","o"]))