#!/usr/bin/python3

# importamos la libreria
import pandas as pd
import numpy as np
import matplotlib as mpl
import matplotlib.pyplot as plt
import seaborn as sns

# para crear el grafico en estas pruebas
#%matplotlib inline

# CREAR UN ARRAY DE 100 NUMEROS ALEATORIOS DE 0-500
# CREAR UN DIAGRAMA DE CAJA
lista_valores = np.random.randint(0,500,100)
print(lista_valores)

print(sns.boxplot(data=lista_valores))
