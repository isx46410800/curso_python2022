#!/usr/bin/python3

# importamos la libreria
import pandas as pd
import numpy as np
import matplotlib as mpl
import matplotlib.pyplot as plt
import seaborn as sns

# para crear el grafico en estas pruebas
#%matplotlib inline

# MAPAS DE CALOR
vuelos = sns.load_dataset("flights")
print(vuelos.head())

# creamos una matriz con los datos:indice,columnas, valor
vuelos = vuelos.pivot("month","year","passengers")

# representacion sin o con valores
print(sns.heatmap(vuelos))
print(sns.heatmap(vuelos,annot=True,fmt="d"))

# mirar un valor concreto
print(vuelos.loc["May"][1956])#318
# hacer un mapa de calor ref valor concreto
valor = vuelos.loc["May"][1956]
print(sns.heatmap(vuelos,center=valor,annot=True,fmt="d",cbar_kws={"orientation":"horizontal"}))
