#!/usr/bin/python3

# importamos el modulo de fecha
from datetime import datetime

fechayhora = datetime.now()
print(fechayhora)

año = fechayhora.year
mes = fechayhora.month
dia = fechayhora.day

hora = fechayhora.hour
minutos = fechayhora.minute
segundos = fechayhora.second
microsegundos = fechayhora.microsecond

print(f"La hora es {hora} : {minutos}")
print(f"La fecha es {dia} {mes} {año}")