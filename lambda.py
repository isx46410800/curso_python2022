#!/usr/bin/python  

# son funciones simples que hacen algo
# pasamos un numero que le sumará 30
resultado = lambda numero: numero + 30
print(resultado(10))

resultado2 = lambda numero1, numero2: numero1 + numero2
print(resultado2(5,8))

# funcion lambda que calcule la media de 3 notas
media = lambda num1,num2,num3: (num1+num2+num3)/3
print(media(5,8,5))