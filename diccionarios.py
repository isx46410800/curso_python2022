#!/usr/bin/python3

# CREANDO DICCIONARIOS
dict = { 'Usuario': 'Miguel', 'Contraseña': 123456, 1 : 2 }
dict2 = { 4 : 5, 3 : 2}

print(dict)
print(type(dict))

# METODOS
## valores de un dict
print(dict.keys())
print(dict.values())
print(dict.items())

## modificando diccionarios
print(dict['Usuario'])
dict['Usuario'] = 'mamoros'
dict.update(dict2)
print(dict)

## añadiento valores al dict
dict['Fecha'] = '14/09/1993'
dict.setdefault(4,5)
print(dict)

## ver un valor
print(dict['Usuario'])
print(dict.get('Usuario'))
## quitar un conjunto
dict.pop('Fecha')
print(dict)

## copiar dict
dict2.copy()
## vaciar diccionario
dict2.clear()
print(dict2)  

# EJERCICIO 1 
dict_paises = {"Guatemala": "Ciudad de Guatemala", "El Salvador": "San Salvador", "Honduras": "Honduras","Nicaragua": "Managua", "Costa Rica": "San Jose", "Panama": "Panama", "Argentina": "Buenos Aires", "Colombia": "Bogota", "Venezuela": "Caracas", "España": "Madrid"}

capital = input("Dime el pais y te digo la capital: ")

capital = capital.capitalize()

if capital in dict_paises.keys():
    print(dict_paises[capital])
else:
    print("Pais no está en la lista")

# EJERCICIO 2
dict_futbolistas = {

    1 : "Casillas", 15 : "Ramos",

    3 : "Pique", 5 : "Puyol",

    11 : "Capdevila", 14 : "Xabi Alonso",

    16 : "Busquets", 8 : "Xavi Hernandez",

    18 : "Pedrito", 6 : "Iniesta",

    7 : "Villa"

}

futbolista = int(input("Dime el numero y te digo el futbolista: "))

if futbolista in dict_futbolistas.keys():
    print(dict_futbolistas[futbolista])
else:
    print("Futbolista no está en la lista")