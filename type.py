#!/usr/bin/python3

num1 = 5
num2 = 5.55
num3, num4 = True , "miguelito"

print(type(num1))
print(type(num2))
print(type(num3))
print(type(num4))

print(float(num1))
print(int(num2))

print("el resultado es:", num1)
print("el resultado es "+ str(num1))
print(f'el resultado es:', num1)
print(f'el resultado es {num1} y {num2}')
print('el resultado es {} y {}'.format(num1,num2))



